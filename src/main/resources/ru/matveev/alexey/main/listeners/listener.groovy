package ru.matveev.alexey.main.listeners

import org.slf4j.LoggerFactory;
def log = LoggerFactory.getLogger(this.getClass())
log.debug("listener {} executed", this.getClass())

