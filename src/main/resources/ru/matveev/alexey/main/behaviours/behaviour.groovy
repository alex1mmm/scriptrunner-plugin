package ru.matveev.alexey.main.behaviours

import com.atlassian.jira.component.ComponentAccessor
import com.onresolve.jira.groovy.user.FieldBehaviours
import groovy.transform.BaseScript
import org.slf4j.LoggerFactory

@BaseScript FieldBehaviours fieldBehaviours

def runScript() {
    def log = LoggerFactory.getLogger(this.getClass())
    log.debug("script {} executed", this.getClass())
}